package com.pawelbanasik.sda_004_pr_add_controls_programatically_butter_knife;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.linear_layout_one)
    protected LinearLayout linearLayoutOne;

    @BindView(R.id.relative_layout_one)
    protected RelativeLayout relativeLayoutOne;

    private int clickCounterForRelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        clickCounterForRelative =0;

    }

    // Przyklad 1 - Linear Layout i dodatkowa funkcja czyli klikanie utworzonego TextView
//    @OnClick(R.id.button_one)
//    public void buttonClick(View v) {
//        final TextView createdTextView = new TextView(getApplicationContext());
//        createdTextView.setText("Nastapilo klikniecie buttonOne");
//
//        createdTextView.setOnClickListener(new View.OnClickListener() {
//            int clickCounterForTextView = 0;
//
//            @Override
//            public void onClick(View v) {
//                createdTextView.setText("Nastapilo klikniecie createdTextView " + (++clickCounterForTextView));
//            }
//        });
//
//        linearLayoutOne.addView(createdTextView);
//    }

    // Przyklad 2 - Relative Layout (trzeba zrobic parametry bo jest relatywny)
    @OnClick(R.id.button_one)
    public void buttonClick() {

        final TextView createdTextView = new TextView(getApplicationContext());
        createdTextView.setText("Nastapilo klikniecie buttonOne");

        // trzy linijki ponizej to zabawa z parametrami tworzonego TextView
        RelativeLayout.LayoutParams parametersOfCreatedTextView = new RelativeLayout.LayoutParams(700, 100);
        parametersOfCreatedTextView.topMargin = 100 * clickCounterForRelative;
        createdTextView.setLayoutParams(parametersOfCreatedTextView);

        createdTextView.setOnClickListener(new View.OnClickListener() {
            int clickCounterForTextView = 0;

            @Override
            public void onClick(View v) {
                createdTextView.setText("Nastapilo klikniecie createdTextView " + (++clickCounterForTextView));
            }
        });

        relativeLayoutOne.addView(createdTextView);

        clickCounterForRelative++;

    }













}
